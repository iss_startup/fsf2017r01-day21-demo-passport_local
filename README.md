# Using passport-local strategy without a database

The project demostrates how you can use **passport-local** strategy with a **users.json** file (instead of a database table) that contains the user login credentials.

This project focuses on the server-side of things; there is no client-side components. To test things out, use **Postman**.

## API Endpoints

### `POST /login` - attempts a log in with username and password
```
POST http://localhost:3000/login

Request body:
{
  "username": "jack",
  "password": "fetch"
}
```

### `GET /home` - the user home page after a successful login
```
GET http://localhost:3000/home
```

### `GET /logout` - logs out the current logged in user
```
GET http://localhost:3000/logout
```

### `GET /api/who` - returns the current logged in user
```
GET http://localhost:3000/api/who
```

