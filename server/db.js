'use strict';

var users = require('./users');

exports.findByUsername = function(username, cb) {
  var isFound = false;
  users.forEach(function(user, idx) {
    if (username == user.username) {
      isFound = true;
      return cb(null, username);
    }
  });
  if (!isFound) {
    return cb(new Error("User '" + username + " does not exist"), null);
  }
};

exports.authenticateUser = function(username, password, done) {
  var isFound = false;
  users.forEach(function(user, idx) {
    if (username == user.username 
    && password == user.password) {
      isFound = true;
      return done(null, username);
    }
  });
  if (!isFound) {
    return done(null, false);
  }
};

