'use strict';

var express = require('express');

module.exports = function(app, passport) {

  app.get('/home', function(req, res) {
    if (!req.isAuthenticated()) {
      res.redirect('/login?src=notloggedin');
    }
    else {
      res.status('200').send('home page of ' + req.user);
    }
  });

  app.get('/login', function(req, res) {
    res.status(202).send('Login Form from "' + req.query.src + '"');
  });

  app.get('/logout', function(req, res) {
    if (!req.isAuthenticated()) {
      res.redirect('/login?src=notloggedin');
    }
    else {
      req.logout();
      req.session.destroy();
      res.redirect("/login?src=logout");
    }
  });

  app.get('/api/who', function(req, res) {
    if (req.isAuthenticated()) {
      res.status(202).send(req.user);
    }
    else {
      res.status(401).send('unknown');
    }
  });

  app.post('/login',
    passport.authenticate('local', {
      failureRedirect: '/login?src=failure'
    }),
    function(req, res) {
      res.redirect('/home');
    }
  );
};