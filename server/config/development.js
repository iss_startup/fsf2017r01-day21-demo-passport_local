'use strict';

module.exports = {
  domain_name: "http://localhost:3000",
  port: 3000,

  session_secret: 'garfield',

  version: '1.0.0'
};
